// bank section
const customerName = document.getElementById('customerName');
const bankBalance = document.getElementById('bankBalance');
const outstandingLoan = document.getElementById('outstandingLoan');
const getLoanBtn = document.getElementById('getLoanBtn');

// work section
const workContainer = document.getElementById('workContainer');
const payBalance = document.getElementById('payBalance');
const sendPayToBankBtn = document.getElementById('sendPayToBankBtn');
const workBtn = document.getElementById('workBtn');
const repayLoanBtn = document.createElement('button');
repayLoanBtn.innerText = 'Repay Loan';

// laptop selector/feature list section
const laptopSelector = document.getElementById('laptopMenu');
const features = document.getElementById('features');

// laptop info section
const laptopInfoContainer = document.getElementById('laptopInfoContainer');
const laptopName = document.getElementById('laptopName');
const description = document.getElementById('description');
const laptopPrice = document.getElementById('laptopPrice');
const laptopImage = document.createElement('img');
const buyNowBtn = document.createElement('button');
buyNowBtn.innerText = 'BUY NOW';

class customer {
    constructor(name, bankBalance, outstandingLoan, payBalance){
        this.name = name;
        this.bankBalance = bankBalance;
        this.outstandingLoan = outstandingLoan;
        this.payBalance = payBalance;
    }

    getLoan(){
        // check if customer already has a loan
        if (this.outstandingLoan > 0){
            window.alert("You must pay back your current loan to get a new one.");
            return;
        }

        let loanRequest = Number(window.prompt("How much money would you like to lend?",""));
        
        // make sure the requested loan amount is a valid number
        if (isNaN(loanRequest)){
            window.alert ("Invalid loan amount.");
            return;
        }

        // check the requested loan amount is not too high
        if (loanRequest > 2 * this.bankBalance){
            window.alert ("Loan amount is too high for your bank balance.");
            return;
        }

        this.bankBalance += loanRequest;
        this.outstandingLoan += loanRequest;

        updateWork();
        updateBank();
    }

    sendPayToBank(){
        // if customer has a loan, pay 10% towards loan
        if (this.outstandingLoan > 0){
            let deduction = Math.min(0.1 * this.payBalance, this.outstandingLoan);
            this.outstandingLoan -= deduction;
            this.payBalance -= deduction;
        }
        
        this.bankBalance += this.payBalance;
        this.payBalance = 0;
        
        updateBank();
        updateWork();
    }

    work(){
        this.payBalance += 100;
        updateWork();
    }

    repayLoan(){
        if (this.payBalance > this.outstandingLoan){
            this.payBalance -= this.outstandingLoan;
            this.outstandingLoan = 0;
            workContainer.removeChild(repayLoanBtn);
        }
        else {
            this.outstandingLoan -= this.payBalance;
            this.payBalance = 0;
        }
        updateBank();
        updateWork();
    }

    buyLaptop(){
        if (currentLaptop.price > this.bankBalance){
            window.alert ("Insufficient funds.");
        }
        else {
            this.bankBalance -= currentLaptop.price;
            window.alert ("You are now the owner of this laptop.");
            updateBank();
        }
    }
}

// example user
const joe = new customer("Joe Banker", 200, 0, 0);

function updateBank(){
    customerName.innerText = joe.name;
    bankBalance.innerText =  `${joe.bankBalance} kr`;
    if (joe.outstandingLoan > 0){
        outstandingLoan.innerText = "Outstanding loan: " + joe.outstandingLoan;
    }
    else {
        outstandingLoan.innerText = "";
    }
}

function updateWork(){
    payBalance.innerText = `${joe.payBalance} kr`;
    if (joe.outstandingLoan > 0){
        workContainer.appendChild(repayLoanBtn);
    }
}

function updateLaptopInfo(){
    switch(laptopSelector.value){
        case "Classic Notebook":
            currentLaptop = laptopsObj[0];
            break;
        case "Box with Screen":
            currentLaptop = laptopsObj[1];
            break;
        case "Heych Pea Office Book":
            currentLaptop = laptopsObj[2];
            break;
        case "SharpEdge Blade":
            currentLaptop = laptopsObj[3];
            break;
        case "The Visor":
            currentLaptop = laptopsObj[4];
            break;
        case "The Egyptian":
            currentLaptop = laptopsObj[5];
            break;
        default:
            console.error("Selected value does not match any laptops") 
    }

    features.innerText = '';

    for (spec of currentLaptop.specs){
        let currentItem = document.createElement('li');
        currentItem.innerText = spec;
        features.appendChild(currentItem);
    }

    laptopName.innerText = currentLaptop.title;
    laptopImage.src = currentLaptop.image
    description.innerText = currentLaptop.description;
    laptopPrice.innerText = `${currentLaptop.price} kr`;

    laptopInfoContainer.appendChild(buyNowBtn);
    laptopInfoContainer.insertBefore(laptopImage, laptopInfoContainer.firstChild);
}

workBtn.addEventListener('click', joe.work.bind(joe));
repayLoanBtn.addEventListener('click', joe.repayLoan.bind(joe));
sendPayToBankBtn.addEventListener('click', joe.sendPayToBank.bind(joe));
getLoanBtn.addEventListener('click', joe.getLoan.bind(joe));
laptopSelector.addEventListener('change', updateLaptopInfo);
buyNowBtn.addEventListener('click', joe.buyLaptop.bind(joe));

let laptopsObj;
let currentLaptop;

async function getLaptops(){
    const response = await fetch("https://noroff-komputer-store-api.herokuapp.com/computers");
    laptopsObj = await response.json();

    for (laptop of laptopsObj){
        let currentItem = document.createElement('option');
        currentItem.innerText = laptop.title;
        laptopSelector.appendChild(currentItem);
    }
}

getLaptops();
updateBank();
updateWork();
# Assignment 4: Komputer Store

'Komputer Store' is a dynamic webpage where you can purchase laptops. You can also see your bank balance, get a loan and work.

The webpage can be seen [here](https://anakstad.gitlab.io/assignment4/).

## Usage

After cloning repository, open index.html in VSCode. Right click and open with LiveServer.


## Contributing
PRs accepted.

[@anakstad](https://gitlab.com/anakstad)
